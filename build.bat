@echo off

rem Uncomment the following line and set it to the path where you have your layers *.dll and *.json
rem set VK_LAYER_PATH=c:\path\to\vulkan\layers

mkdir build
pushd build
cl /Od /Zi ..\main.cpp user32.lib
popd

glslangValidator -V simple.vert
glslangValidator -V simple.frag